﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Siccity.GLTFUtility;


// Model Loader Class
/*
 * 
 */
public class ModelLoader : MonoBehaviour
{
    // Singleton instance
    public static ModelLoader Instance;

    bool _isDownloading;
    public float Progress { get; private set; }
    static string _rootPath;
    string filePath;

    [SerializeField]
    private Dictionary<ModelType, GameObject> _modelDict;

    private void Awake()
    {
        Instance = this;

        _modelDict = new Dictionary<ModelType, GameObject>();

#if UNITY_EDITOR
        _rootPath = $"{Application.streamingAssetsPath}/Models";
#else
        _rootPath = $"{Application.persistentDataPath}/Models";
#endif
    }

    void DownloadFromURL(ModelType type, string url, Action<GameObject> callback)
    {
        if (_isDownloading)
        {
            Debug.LogError("Already downloading a file");
            return;
        }

        UIStateMachine.Instance.ChangeState(UIStateEnum.Download);

        StartCoroutine(GetFileRequest(url, (UnityWebRequest req) =>
        {
            UIStateMachine.Instance.ChangeState(UIStateEnum.Select);
            Debug.Log($"request is done? {req.isDone}");
            if (req.isNetworkError || req.isHttpError)
            {
                // Log any errors that may happen
                Debug.Log($"[unity] unity ERROR {req.error} : {req.downloadHandler.text}");
            }
            else
            {
                // Load the model from disk and return a copy
                /*AddModelToDict(type, filePath, (GameObject model) => {
                    callback(Instantiate(model));
                });*/
                StartCoroutine(AddModelToDictAsync(type, filePath, (GameObject model) => {
                    callback(Instantiate(model));
                }));
            }
        }));
    }

    IEnumerator GetFileRequest(string url, Action<UnityWebRequest> callback)
    {
        _isDownloading = true;
        Debug.Log("Downloading...");

        using (UnityWebRequest req = UnityWebRequest.Get(url))
        {
            // Save model in a local file
            req.downloadHandler = new DownloadHandlerFile(filePath);

            StartCoroutine(TrackDownloadProgress(req));
            yield return req.SendWebRequest();

            _isDownloading = false;
            Debug.Log("Download finished");
            yield return new WaitForSeconds(0.01f);
            callback(req);
        }
    }

    // Updates the value of the progress bar
    IEnumerator TrackDownloadProgress(UnityWebRequest req)
    {
        Debug.Log("progress started");
        while (!req.isDone)
        {
            Progress = req.downloadProgress;
            yield return null;
        }
        Debug.Log("progress ended");

        yield break;
    }

    public void LoadModel(ModelType type, string url, Action<GameObject> callback)
    {
        filePath = GetFilePath(url);

        // if we have already loaded the model type, return a copy
        if (_modelDict.ContainsKey(type))
        {
            callback(Instantiate(_modelDict[type]));
            return;
        }

        // if the model has been downloaded but not loaded from disk, load it and store it in the dictionary
        if (File.Exists(filePath))
        {
            Debug.Log("File already exists, loading...");

            // Load the model from disk and return a copy
            /*AddModelToDict(type, filePath, (GameObject model) => {
                callback(Instantiate(model));
            });*/

            StartCoroutine(AddModelToDictAsync(type, filePath, (GameObject model) => {
                callback(Instantiate(model));
            }));
        }
        else
        {
            // Download the model from the web
            DownloadFromURL(type, url, callback);
        }
    }

    string GetFilePath(string url)
    {
        return _rootPath + url.Substring(url.LastIndexOf('/'));
    }

    void AddModelToDict(ModelType type, string filePath, Action<GameObject> callback)
    {
        /*
        GameObject model = Importer.LoadFromFile(filePath);
        model.SetActive(false);
        _modelDict.Add(type, model);
        callback(model);
        */

        
        Importer.ImportGLBAsync(filePath, new ImportSettings(), (GameObject result, AnimationClip[] anims) =>
        {
            result.SetActive(false);
            _modelDict.Add(type, result);
            callback(result);
        });
        
    }

    IEnumerator AddModelToDictAsync(ModelType type, string filePath, Action<GameObject> callback)
    {
        bool isLoaded = false;
        GameObject model = null;

        Importer.ImportGLBAsync(filePath, new ImportSettings(), (GameObject result, AnimationClip[] anims) =>
        {
            isLoaded = true;
            model = result;
        });
        
        while (!isLoaded)
        {
            yield return new WaitForSeconds(0.1f);
        }

        model.SetActive(false);
        _modelDict.Add(type, model);
        callback(model);
    }
}
