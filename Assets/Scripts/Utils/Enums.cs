﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModelType
{
    Bot = 0,
    Helmet,
    Katana,
    Plant
}

public enum UIStateEnum
{
    Call2Action = 0,
    Select,
    Download,
}
