﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIState : State
{
    // UI Elements to control (show/hide)
    public List<GameObject> UIElements;

    public override void OnEnterState()
    {
        base.OnEnterState();
        StartCoroutine(ShowElements());
    }

    public override void OnExitState()
    {
        base.OnExitState();
        StartCoroutine(HideElements());
    }
    protected IEnumerator HideElements()
    {
        active = false;

        foreach (GameObject element in UIElements)
        {
            element.SetActive(false);
        }

        yield return null;
    }

    protected IEnumerator ShowElements()
    {
        active = false;

        timeInState = 0;
        foreach (GameObject element in UIElements)
        {
            element.SetActive(true);
        }

        yield return null;

        active = true;
    }
}
