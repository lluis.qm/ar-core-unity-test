﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    Camera _cam;
    private void Awake()
    {
        if (_cam == null) _cam = Camera.main;
    }
    void Update()
    {
        transform.LookAt(_cam.transform);
    }
}
