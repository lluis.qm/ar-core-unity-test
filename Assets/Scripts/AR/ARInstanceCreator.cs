﻿using System.Collections.Generic;
using UnityEngine;

public class ARInstanceCreator : MonoBehaviour
{
    public float distance = 1.572f; // Distance from camera to instantiate models

    public static ARInstanceCreator Instance;

    [SerializeField]
    private Camera arCamera;

    public GameObject virtualAnchor;

    [SerializeField]
    GameObject _instancePrefab;
    bool _creatingInstance;

    [SerializeField]
    List<ReferenceObject> m_ReferenceObjects;
    // Object Pooling
    Dictionary<ModelType, List<ReferenceObject>> m_InactiveInstances;

    [SerializeField]
    ReferenceObject _selectedObject;

    void Awake()
    {
        Instance = this;

        if (arCamera == null) arCamera = Camera.main;
        m_ReferenceObjects = new List<ReferenceObject>();
        m_InactiveInstances = new Dictionary<ModelType, List<ReferenceObject>>();

        m_InactiveInstances.Add(ModelType.Bot, new List<ReferenceObject>());
        m_InactiveInstances.Add(ModelType.Helmet, new List<ReferenceObject>());
        m_InactiveInstances.Add(ModelType.Katana, new List<ReferenceObject>());
        m_InactiveInstances.Add(ModelType.Plant, new List<ReferenceObject>());
    }

    // Gathers user input and performs actions
    void Update()
    {
        Vector3 touchPosition;

#if UNITY_EDITOR
        // DEBUGGING in Editor ONLY
        // Use mouse button instead of touches
        if (Input.GetMouseButtonDown(0))
        {
            touchPosition = Input.mousePosition;
        } else
        {
            return;
        }
#else
        // If there is no tap or nothing to tap, do nothing
        if (Input.touchCount == 0 || m_ReferenceObjects.Count == 0)
            return;

        var touch = Input.GetTouch(0);
        touchPosition = touch.position;
        if (touch.phase != TouchPhase.Began)
            return;
#endif
        // Raycast to check for selected instances
        Ray ray = arCamera.ScreenPointToRay(touchPosition);
        RaycastHit hitObject;
        if(Physics.Raycast(ray, out hitObject))
        {
            if (hitObject.transform.CompareTag("Reference"))
            {
                ReferenceObject refObject = hitObject.transform.GetComponent<ReferenceObject>();
                if (refObject != null)
                {
                    DisselectAllInstances();
                    SelectInstance(refObject);
                }
            }
        }
    }

    public void CreateInstance(ModelType type, string name, string url)
    {
        if (_creatingInstance) return;

        // If we already have an inactive instance of the same type, use that
        if (m_InactiveInstances[type].Count > 0)
        {
            List<ReferenceObject> inactiveObjects = m_InactiveInstances[type];

            ReferenceObject refObject = inactiveObjects[inactiveObjects.Count - 1];
            refObject.transform.position = Camera.main.transform.TransformPoint(Vector3.forward * distance);
            refObject.transform.LookAt(Camera.main.transform);
            refObject.gameObject.SetActive(true);

            inactiveObjects.RemoveAt(inactiveObjects.Count - 1);
            m_ReferenceObjects.Add(refObject);
            return;
        }

        // Create new instance instead
        Debug.Log("Creating instance");
        _creatingInstance = true;

        // Instantiate an empty Reference Object which will hold the model
        ReferenceObject instance = Instantiate(_instancePrefab).transform.GetComponent<ReferenceObject>();
        instance.gameObject.name = name;
        instance.type = type;
        // Set reference to the canvas in AR space
        instance.SetAnchorAR(virtualAnchor);
        m_ReferenceObjects.Add(instance);

        ModelLoader.Instance.LoadModel(type, url, (GameObject go) =>
        {
            go.transform.parent = instance.transform;
            go.transform.position = instance.transform.position;
            go.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            // Activate the received copy of the gameobject
            go.SetActive(true);

            // With the model object attached as a child, compute the bounding box for selection Raycast
            instance.ComputeBoundingBox();
            instance.transform.position = Camera.main.transform.TransformPoint(Vector3.forward * distance);
            instance.transform.LookAt(Camera.main.transform);

            _creatingInstance = false;
        });
    }

    public int GetInstanceCount()
    {
        return m_ReferenceObjects.Count;
    }

    // Removes currently selected instance
    public void RemoveSelectedInstance()
    {
        if (_selectedObject == null) return;

        m_ReferenceObjects.Remove(_selectedObject);

        _selectedObject.Disselect();
        //Destroy(_selectedObject.gameObject);
        _selectedObject.gameObject.SetActive(false);
        m_InactiveInstances[_selectedObject.type].Add(_selectedObject);

        _selectedObject = null;
    }

    // Removes all the instances created in the scene
    public void RemoveAllInstances()
    {
        Debug.Log("remove all created instances");
        DisselectAllInstances();

        foreach (var referenceObject in m_ReferenceObjects)
        {
            //Destroy(referenceObject.gameObject);
            referenceObject.Disselect();
            referenceObject.gameObject.SetActive(false);
            m_InactiveInstances[referenceObject.type].Add(referenceObject);
        }
        m_ReferenceObjects.Clear();
    }

    public void SelectInstance(ReferenceObject selected)
    {
        Debug.Log("selecting instance");
        selected.Select();
        _selectedObject = selected;
    }

    void DisselectAllInstances()
    {
        foreach (ReferenceObject reference in m_ReferenceObjects)
        {
            reference.Disselect();
        }
    }
}
