﻿using System;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


// Reference Object in an AR space
public class ReferenceObject : MonoBehaviour
{
    public ModelType type;
    ARReferencePoint _refPoint;
    BoundingBoxCollider _bBox;
    [SerializeField]
    bool _isSelected = false;

    private GameObject _anchor;

    private void Awake()
    {
        _bBox = GetComponent<BoundingBoxCollider>();
    }

    public TrackableId GetTrackableId()
    {
        return _refPoint.trackableId;
    }

    public void SetAnchorAR(GameObject vAnchor)
    {
        _anchor = vAnchor;
    }

    public void Select()
    {
        if (_isSelected) return;

        _isSelected = true;

        try
        {
            _anchor.transform.position = _bBox.GetBottom();
            _anchor.gameObject.SetActive(true);
        } catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }

    public void Disselect()
    {
        if (!_isSelected) return;

        _isSelected = false;

        try
        {
            _anchor.gameObject.SetActive(false);
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }

    public void ComputeBoundingBox()
    {
        _bBox.SetBoxCollider();
    }
}
