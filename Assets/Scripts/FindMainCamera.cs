﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindMainCamera : MonoBehaviour
{
    Canvas _canvas;

    private void Awake()
    {
        _canvas = GetComponent<Canvas>();
        if (_canvas.worldCamera == null) _canvas.worldCamera = Camera.main;
    }
}
