﻿using UnityEngine;
using UnityEngine.UI;

public class InstanceCounter : MonoBehaviour
{
    Text display;

    private void Awake()
    {
        display = GetComponent<Text>();
    }

    void Update()
    {
        display.text = $"{ARInstanceCreator.Instance.GetInstanceCount()} instances";
    }
}
