﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelSelector : MonoBehaviour
{
    public ModelType modelType;
    public string modelName;
    public string modelURL;
    Text _text;
    Button _button;

    private void Awake()
    {
        _text = GetComponentInChildren<Text>();
        _text.text = modelName;
        _button = GetComponent<Button>();

        _button.onClick.AddListener(SelectModel);
    }

    private void SelectModel()
    {
        Debug.Log($"You selected model {modelName}");
        ARInstanceCreator.Instance.CreateInstance(modelType, modelName, modelURL.Trim());
    }
}
