﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Call2Action : UIState
{
    public float splashTime = 2.0f;
    public UIStateEnum nextState = UIStateEnum.Select;
    public override void UpdateState()
    {
        base.UpdateState();
        if (timeInState >= splashTime)
        {
            UIStateMachine.Instance.ChangeState(nextState);
        }
    }
}
