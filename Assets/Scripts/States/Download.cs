﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Download : UIState
{
    public Slider progressBar;

    public override void UpdateState()
    {
        base.UpdateState();

        progressBar.value = ModelLoader.Instance.Progress;
    }
}
